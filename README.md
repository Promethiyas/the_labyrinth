# Labyrinthe

## Bienvenue dans le labyrinthe

### CAHIER DES CHARGES

Pour un projet de developpement nous avons décidé de réaliser un jeu où nous incarnons un personnage enfermé dans un labyrinthe où rôde une créature.
Le joueur pourra récupérer trois clés dans trois donjons différents ainsi qu'une quatrieme sur un ennemi afin de pouvoir ouvrir la porte permettant de s'échapper, car le but n'est pas terrasser la créature mais de s'échapper du labyrinthe.

### LOGICIEL

Nous utiliserons Unreal Engine 5 car nous venons de sortir d'un cours de GameProg dans lequel nous utilisions UE5.

### HUD

 - barre de vie
 - nombre de clé
 - nombre de pièces (n'apparait qu'une fois que nous avons récupéré une pièce)
 - quêtes
 - informations d'aide
     - touches
     - explications

### IA

 - la créature peut voir le joueur et le suivre, sinon elle se ballade aléatoirement et explore le labyrinthe
 - ennemis de l'arène

### INTERACTIONS

 - porte (à la fin)
 - portes de donjons
 - les clés
 - les coffres
### MOUVEMENTS POSSIBLE

 - courir 
 - sauter

### SYSTEME DE JEU

 - vie
 - dégats
 - quêtes 
 - clés
 - pièces

### ENNEMIS
 
  - créature qui nous poursuit
  - squelettes (qui drop la clé)
  - ennemis de l'arène

### BDD

 - sauvegarde derrière coord du joueur pour que quand il revient il respawn au dernier endroit
 - sauvegarde amélioration de map
 - 2 tables
     - player
         - vie
         - nombre de clés
         - nombre de pièces
         - dernière position
     - items
         - id
         - name
         - type
         - description

### EVENEMENTS 

 - ennemis apparaissent dans zone prédéfinie et qui nous attaquent
 - la porte finale qui s'ouvre avec les 4 clés
 - ouverture du coffre qui permet d'obtenir un succès

